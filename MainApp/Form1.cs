﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MainApp
{
    public partial class Form1 : Form
    {
        private static string Path = "";
        private static string Filter = "";
        private System.IO.FileSystemWatcher FileMonitor;

        List<string> readFiles = new List<string>();
        List<string> decryptedFiles = new List<string>();

        delegate void SetTextCallback(string text);
        TcpListener listener;
        TcpListener listener2;
        TcpClient client;
        TcpClient client2;

        Queue<Klijent> koderi;
        Queue<Klijent> dekoderi;

        Queue<NetworkStream> ns;
        List<NetworkStream> listns;
        Queue<NetworkStream> ns2;
        List<NetworkStream> listns2;
        Thread t = null;
        Thread t2 = null;
        Thread tt = null;
        Thread tt2 = null;
        public Form1()
        {
            InitializeComponent();

            this.FileMonitor = new System.IO.FileSystemWatcher();
            ((System.ComponentModel.ISupportInitialize)(this.FileMonitor)).BeginInit();
            this.FileMonitor.EnableRaisingEvents = true;
            this.FileMonitor.NotifyFilter = System.IO.NotifyFilters.FileName;
            this.FileMonitor.SynchronizingObject = this;
            this.FileMonitor.Created += new System.IO.FileSystemEventHandler(this.FileMonitor_Changed);
            ((System.ComponentModel.ISupportInitialize)(this.FileMonitor)).EndInit();

            SetReadFiles();

            ns = new Queue<NetworkStream>();
            ns2 = new Queue<NetworkStream>();

            koderi = new Queue<Klijent>();
            dekoderi = new Queue<Klijent>();

            listener = new TcpListener(4545);
            listener.Start();
            tt = new Thread(AcceptWork);
            tt.Start();
            listns = new List<NetworkStream>();
           

            listener2 = new TcpListener(5454);
            listener2.Start();
            tt2 = new Thread(AcceptWork2);
            tt2.Start();
            listns2 = new List<NetworkStream>();
            
        }


        #region FSW
        private void FileMonitor_Changed(object sender, System.IO.FileSystemEventArgs e)
        {
            string ChangeType = e.ChangeType.ToString();

            if (ChangeType == "Created")
            {
                String s = e.FullPath;
                readFiles.Add(e.FullPath);
                byte[] byteTime = Encoding.ASCII.GetBytes(s);
                Klijent pomk = koderi.Dequeue();
                NetworkStream pom = pomk.NetworkStream;
                //ns.Dequeue();
                //                ns.Enqueue(pom);
                koderi.Enqueue(pomk);

                pom.Write(byteTime, 0, byteTime.Length);

                string pathDecript = @"D:\todecrypt";

                if (IsDirectoryNotEmpty(System.IO.Path.GetDirectoryName(pathDecript)))
                {
                    string file = FindFirstFile(pathDecript, "");
                    if(file!=String.Empty)
                    {
                        decryptedFiles.Add(file);
                        byte[] byteTime1 = Encoding.ASCII.GetBytes(file);
                        Klijent pomd = dekoderi.Dequeue();
                        NetworkStream pom1 = pomd.NetworkStream;
                        //ns2.Dequeue();
                        //ns2.Enqueue(pom1);
                        dekoderi.Enqueue(pomd);
                        pom1.Write(byteTime1, 0, byteTime1.Length);
                    }
                }
            }

        }

        public bool IsDirectoryNotEmpty(string path)
        {
            return Directory.EnumerateFileSystemEntries(path).Any();
        }

        public string FindFirstFile(string path, string searchPattern)
        {
            string[] files;

            try
            {
                files = Directory.GetFiles(path);
            }
            catch (Exception)
            {
                return string.Empty;
            }

            if (files.Length > 0)
            {
                foreach(string f in files)
                {
                    if (!decryptedFiles.Contains(f))
                        return f;
                }
            }
            
            return string.Empty;
        }

        private void btnStartFSW_Click(object sender, EventArgs e)
        {
            Path = @"D:\toencrypt";
            Filter = "*.*";

            FileMonitor.Path = Path.ToString();
            FileMonitor.Filter = Filter.ToString();
            FileMonitor.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite
                | NotifyFilters.FileName | NotifyFilters.DirectoryName;

            FileMonitor.EnableRaisingEvents = true; //pocinje FSW kodiranja

            List<string> listaDodatih = new List<string>();
            string path = @"D:\toencrypt";
            listaDodatih = FindAllAddedFIles(path);
            if(listaDodatih.Count>0)
            {
                foreach (string s in listaDodatih)
                {
                    readFiles.Add(s);
                    byte[] byteTime = Encoding.ASCII.GetBytes(s);
                    Klijent pomk = koderi.Dequeue();
                    NetworkStream pom = pomk.NetworkStream;
                    //ns.Dequeue();
                    //ns.Enqueue(pom);
                    koderi.Enqueue(pomk);
                    pom.Write(byteTime, 0, byteTime.Length);
                    
                }
            }
        }

        private void SetReadFiles()
        {
            var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

            var fi = new System.IO.FileInfo(@"D:\Fakultet\4. godina\7. semestar\Zastita informacija\Projekat\ZI - 15387\readfiles.bin");

            if (fi.Exists)
            {
                using (var binaryFile = fi.OpenRead())
                {
                    if (binaryFile.Length > 0)
                        readFiles = (List<string>)binaryFormatter.Deserialize(binaryFile);
                    else
                        readFiles = new List<string>();
                }
            }
            else
            {
                readFiles = new List<string>();
            }
        }

        private List<string> FindAllAddedFIles(string path)
        {
            string[] files;
            List<string> dodati = new List<string>();

            try
            {
                files = Directory.GetFiles(path);
            }
            catch (Exception)
            {
                return null;
            }

            if (files.Length > 0)
            {
                foreach (string f in files)
                {
                    if (!readFiles.Contains(f))
                        dodati.Add(f);
                }
            }

            return dodati;
        }

        private void btnStopFSW_Click(object sender, EventArgs e)
        {
            FileMonitor.EnableRaisingEvents = false;
            SaveReadFiles();
        }

        private void SaveReadFiles()
        {
            var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

            var fi = new System.IO.FileInfo(@"D:\Fakultet\4. godina\7. semestar\Zastita informacija\Projekat\ZI - 15387\readfiles.bin");

            using (var binaryFile = fi.Create())
            {
                binaryFormatter.Serialize(binaryFile, readFiles);
                binaryFile.Flush();
            }
        }

        #endregion

        #region communication
        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        public void AcceptWork()
        {
            while (true)
            {
                client = listener.AcceptTcpClient();
                NetworkStream n1 = client.GetStream();
                byte[] bytes = new byte[1024];
                int bytesRead = n1.Read(bytes, 0, bytes.Length);
                Klijent k = new Klijent(int.Parse(Encoding.ASCII.GetString(bytes, 0, bytesRead)), n1, "k");
                //ns.Enqueue(n1);
                koderi.Enqueue(k);
            }
        }
        public void AcceptWork2()
        {
            while (true)
            {
                client2 = listener2.AcceptTcpClient();
                NetworkStream n2 = client2.GetStream();
                byte[] bytes = new byte[1024];
                int bytesRead = n2.Read(bytes, 0, bytes.Length);
                Klijent k = new Klijent(int.Parse(Encoding.ASCII.GetString(bytes, 0, bytesRead)), n2, "d");
                //ns2.Enqueue(n2);
                dekoderi.Enqueue(k);
            }
        }

        public System.Threading.ThreadStart DoWork(NetworkStream n)
        {
            byte[] bytes = new byte[1024];
            while (true)
            {

                int bytesRead = n.Read(bytes, 0, bytes.Length);
                this.SetText(Encoding.ASCII.GetString(bytes, 0, bytesRead));
                //MessageBox.Show(Encoding.ASCII.GetString(bytes, 0, bytesRead));
            }
        }
   
        public System.Threading.ThreadStart DoWork2(NetworkStream n)
        {
            byte[] bytes = new byte[1024];
            while (true)
            {

                int bytesRead = n.Read(bytes, 0, bytes.Length);
                this.SetText2(Encoding.ASCII.GetString(bytes, 0, bytesRead));
                //MessageBox.Show(Encoding.ASCII.GetString(bytes, 0, bytesRead));
            }
        }
        private void SetText(string text)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            
        }
        private void SetText2(string text)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
        }

        #endregion
    }
}
