﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Decypher
{
    public partial class RC6Key : Form
    {
        public TextBox key;
        public RC6Key()
        {
            InitializeComponent();
            key = tbKey;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
        }
    }
}
