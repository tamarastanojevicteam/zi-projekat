﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decypher
{
    public class A3_TEA_PCBC
    {
        private static int[] key;
        private int[] IV, IVpom;
        private static int delta;


        public A3_TEA_PCBC()
        {

        }

        public A3_TEA_PCBC(byte[] k, byte[] iv)
        {
            key = new int[4];
            key = new int[k.Length / 4];
            for (int i = 0; i < k.Length; i += 4)
                key[i / 4] = BitConverter.ToInt32(k, i);


            IV = new int[2];
            IVpom = new int[2];
            IV = new int[iv.Length / 4];
            for (int i = 0; i < iv.Length; i += 4)
                IV[i / 4] = BitConverter.ToInt32(iv, i);


            IVpom[0] = IV[0];
            IVpom[1] = IV[1];

            delta = unchecked((int)0x9e3779b9);
        }

        public byte[] decryptBlock(byte[] input)
        {
            int[] pom = new int[2];
            int[] cipher = new int[2];
            cipher = new int[input.Length / 4];
            for (int i = 0; i < input.Length; i += 4)
                cipher[i / 4] = BitConverter.ToInt32(input, i);
            

            int v0 = pom[0] = cipher[0];
            int v1 = pom[1] = cipher[1];

            int sum = delta << 5;

            for (int i = 0; i < 32; i++)
            {
                v1 -= ((v0 << 4) + key[2]) ^ (v0 + sum) ^ ((v0 >> 5) + key[3]);
                v0 -= ((v1 << 4) + key[0]) ^ (v1 + sum) ^ ((v1 >> 5) + key[1]);
                sum -= delta;
            }

            cipher[0] = v0 ^ IVpom[0];
            cipher[1] = v1 ^ IVpom[1];

            IVpom[0] = pom[0] ^ cipher[0];
            IVpom[1] = pom[1] ^ cipher[1];

            byte[] data = new byte[cipher.Length * 4];
            for (int i = 0; i < cipher.Length; i++)
                Array.Copy(BitConverter.GetBytes(cipher[i]), 0, data, i * 4, 4);

            return data;

        }

        public byte[] decrypt(byte[] data)
        {

            IVpom[0] = IV[0];
            IVpom[1] = IV[1];

            byte[] tmp = new byte[data.Length];
            byte[] block = new byte[8];

            int i;
            for (i = 0; i < data.Length; i++)
            {
                if (i > 0 && i % 8 == 0)
                {
                    block = decryptBlock(block);
                    Array.Copy(block, 0, tmp, i - 8, block.Length);
                }

                if (i < data.Length)
                    block[i % 8] = data[i];
            }

            block = decryptBlock(block);
            Array.Copy(block, 0, tmp, i - 8, block.Length);

            tmp = deletePadding(tmp);
            return tmp;
        }

        private static byte[] deletePadding(byte[] input)
        {
            int count = 0;

            int i = input.Length - 1;
            while (input[i] == 0)
            {
                count++;
                i--;
            }

            byte[] tmp = new byte[input.Length - count - 1];
            Array.Copy(input, 0, tmp, 0, tmp.Length);
            return tmp;
        }

    }
}
